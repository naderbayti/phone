import { Offcanvas } from "react-bootstrap";
import MenuProvider from "../../../Stores/Context/MenuContext";
import Menu from "./Menu";


const SideBar = ({ show }) => {
    return (<Offcanvas backdrop={false} show={show} scroll={true} placement="start" >
        <Offcanvas.Header >

            <Offcanvas.Title>
                <h6> مدیریت تماس</h6>
            </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
            <MenuProvider>
                <Menu />
            </MenuProvider>
        </Offcanvas.Body>
    </Offcanvas>)
}
export default SideBar;