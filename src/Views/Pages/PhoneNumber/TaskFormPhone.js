import { useEffect, useState } from 'react'
import { Container, Row, Col } from 'react-bootstrap';


const TaskFormPhone = (props) => {





    const [task, setTask] = useState(props.task);

    useEffect(() => {
        setTask({ ...props.task });
    }, [props.task]);

    const handleChange = (e) => {
        setTask({ ...task, [e.target.id]: e.target.value })
    }

    const handleChangeNumber = (e) => {
        setTask({ ...task, [e.target.id]: Number(e.target.value) })
    }

    const resetForm = () => {
        setTask({ id: 0, taskName: '', duration: 0, statusId: 1 });
    }

    return (<form>
        <Container fluid >
            <Row className="p-2">
                <Col> <label htmlFor="name"><b>نام:</b></label>

                    <input id="name" className="form-control" value={task.name} type="text" onChange={handleChange} />
                </Col>
                <Col><label htmlFor="fname"><b>نام خانوادگی:</b></label>

                    <input id="fname" className="form-control" value={task.fname} type="text" onChange={handleChange} />
                </Col>
            </Row>
            <Row className="p-2">
                <Col> <label htmlFor="phonenumber"><b>شماره همراه:</b></label>

                    <input id="phonenumber" className="form-control" value={task.phonenumber} type="number" onChange={handleChangeNumber} />
                </Col>
                <Col>  <label htmlFor="address"><b>آدرس:</b></label>

                    <input id="address" className="form-control" value={task.address} type="text" onChange={handleChange} />
                </Col>
            </Row>
            <Row className="p-2">
                <Col>
                    <button type="button" className="btn btn-success" style={{ width: 150 }} onClick={
                        () => {
                            if (props.registerClick) {
                                props.registerClick(task);
                                resetForm();
                            }
                        }
                    }>ثبت</button></Col>

            </Row>
        </Container>

    </form >)
}

export default TaskFormPhone;