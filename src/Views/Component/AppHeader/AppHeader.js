import { Container, Navbar, NavbarBrand } from "react-bootstrap";




const AppHeader = ({ toggleButtn }) => {

    return (
        <Navbar bg="light" expand={false}>
            <Container fluid style={{ direction: "ltr" }}>

                <NavbarBrand href="#"></NavbarBrand>
                <Navbar.Toggle aria-controls="offcanvasNavbar" onClick={toggleButtn} />
            </Container>

        </Navbar>
    )
}
export default AppHeader;