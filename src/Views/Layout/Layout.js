

import React, { useState } from "react";
import { Stack } from "react-bootstrap";
import AppContent from "../Component/AppContent/AppContent";
import AppHeader from "../Component/AppHeader/AppHeader";
import AppFooter from "../Component/AppFooter/AppFooter";
import SideBar from "../Component/Sidebar/SideBar";


const Layout = () => {

    const [showSideBar, setShowSidebar] = useState(true);
    const handelSideBar = () => {
        setShowSidebar(!showSideBar);
    }
    return (
        <>

            <Stack gap={1} className="main" style={{ marginRight: showSideBar ? 230 : 0 }}>

                <AppHeader toggleButtn={handelSideBar} />
                <AppContent />
                <AppFooter />
            </Stack>
            <SideBar show={showSideBar} />
        </>
    )
}
export default Layout;