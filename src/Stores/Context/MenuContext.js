import { createContext, useState } from "react";

export const MenuContext = createContext(null);

const MenuProvider = ({ children }) => {

    const menuList = [
        {
            title: "داشبورد", icon: "fa fa-home",
            subMenus: [{ title: 'صفحه اصلی', icon: "fa fa-home", to: "HomePage" }]

        }

        ,
        // {
        //     title: 'دفتر تلفن', icon: 'fa fa-product-hunt',
        //     subMenus: [{ title: ' مدیریت تلفن', icon: "fa fa-product-hunt", to: "PhoneNumber" }

        //     ]


        // },
        // {
        //     title: 'درباره ی ما', icon: 'fa fa-user-o',
        //     subMenus: [
        //         { title: 'درباره ی ما', icon: "fa fa-list-alt", to: "AboutUs" },
        //     ]


        // },

        { title: 'راهنما', icon: 'fa fa-question' }
    ];

    const [activeMenu, setActiveMenu] = useState(-1);

    const selectMenu = (index) => {
        if (index === activeMenu) {
            setActiveMenu(-1);
        }
        else
            setActiveMenu(index);
    }

    return (<MenuContext.Provider value={{ selectMenu, activeMenu, menuList }}>
        {children}
    </MenuContext.Provider>)
}

export default MenuProvider;