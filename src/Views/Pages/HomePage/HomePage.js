import { useState } from "react";
import { Nav } from "react-bootstrap";
import AboutUs from "../About Us/AboutUs";
import './HomePage.css'
import PhonePage from "../PhoneNumber/PhonePage";

const HomePage = () => {
    const [activeTab, setActiveTab] = useState(1);
    return (
        <div className="HomePage">
            <h4>صفحه اصلی</h4>
            <hr />
            <Nav variant="tabs" defaultActiveKey={activeTab} style={{ direction: "rtl" }}>
                <Nav.Item>
                    <Nav.Link eventKey="1" onClick={() => { setActiveTab(1) }}>اطلاعات تماس</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="2" onClick={() => { setActiveTab(2) }}>درباره ی ما</Nav.Link>
                </Nav.Item>

            </Nav>


            {
                activeTab === 1 ? <PhonePage /> : <AboutUs />
            }
           

        </div>



    )
}
export default HomePage;