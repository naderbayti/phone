import './PhoneNumber.css';

const TaskListView = (props) => {

    const { list } = props;

    return (


        <table className="table mt-2">
            <thead>
                <tr >
                    <th>نام</th>
                    <th>نام خانوادگی</th>
                    <th>تلفن همراه</th>
                    <th>آدرس</th>
                    <th><span className="fa fa-gear"></span></th>
                </tr>
            </thead>
            <tbody>
                {
                    list.map((value) => {

                        return (<tr key={value.id}>
                            <td>{value.name}</td>
                            <td>{value.fname}</td>
                            <td>{value.phonenumber}</td>
                            <td>{value.address}</td>
                            <td >
                                <i className="fa fa-edit text-success" style={{ marginLeft: 5 }}
                                    onClick={() => {
                                        if (props.editClick) {
                                            props.editClick(value);
                                        }
                                    }}
                                ></i>
                                <i className="fa fa-remove text-danger"
                                    onClick={() => {
                                        if (props.removeClick) {
                                            props.removeClick(value);
                                        }
                                    }}
                                ></i>
                            </td>

                        </tr>)
                    })
                }
            </tbody>
        </table>
    )
}

export default TaskListView;