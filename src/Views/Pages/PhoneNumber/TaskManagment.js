import { useState } from "react";
import TaskListView from './TaskListView'
import TaskFormPhone from "./TaskFormPhone";
import Modal from "../../../Components/Modal/Modal";
import { successMessage } from "../../../Utils/Alert/alert";

const TaskManagment = (props) => {


    const [showModal, setShowModal] = useState(false);

    const [selectedTask, setSelectedTask] =
        useState({ id: 0, name: '', fname: '', phonenumber: 0, address: '' })

    const [taskList, setTaskList] = useState([]);

    const register = (task) => {

        if (task.id === 0) {
            // Register ...
            let id = 1;
            if (taskList.length > 0) {
                id = taskList[taskList.length - 1] + 1
            }
            task.id = id;

            setTaskList([...taskList, task]);
        }
        else {
            // Edit ...
            let index = taskList.findIndex(p => p.id === task.id);
            if (index !== -1) {
                taskList[index] = task;
                setTaskList([...taskList]);
            }
        }



        setShowModal(false);

        successMessage();

    }

    const remove = (task) => {

        let index = taskList.findIndex(p => p.id === task.id);
        if (index !== -1) {
            let temp = [...taskList];
            temp.splice(index, 1);
            setTaskList([...temp]);
        }
    }

    const edit = (task) => {

        setShowModal(true);
        setSelectedTask(task);
    }

    return (
        <div className="p-2">
            <h4>مخاطبین</h4>
            <hr />
            <button className="btn  btn-success" onClick={() => {
                setShowModal(true);
                setSelectedTask({ id: 0, name: '', fname: '', phonenumber: 0, address: '' })
            }}><span className="fa fa-plus" style={{ marginRight: 5 }}></span>افزودن مخاطب</button>

            <TaskListView
                removeClick={remove} editClick={edit}
                list={taskList} />
            <Modal
                header={<p>فرم اطلاعات</p>}
                show={showModal} hide={() => { setShowModal(false) }}>
                <TaskFormPhone
                    task={selectedTask}
                    registerClick={register} />
            </Modal>

        </div>
    )
}

export default TaskManagment;

